<?php

/**
 * @file
 * Enables SWF Tools support for SimpleViewer.
 */

/**
 * Implements hook_swftools_methods().
 */
function swftools_simpleviewer_swftools_methods() {
  $simpleviewer = array(
    'module'      => 'swftools_simpleviewer',
    'title'       => t('SimpleViewer'),
    'download'    => 'http://www.simpleviewer.net/simpleviewer',
    'library'     => swftools_get_library_path('simpleviewer') . '/simpleviewer.swf',
    'profile'     => array(
      'path' => 'simpleviewer',
      'settings' => array('swftools_simpleviewer'),
      'file' => 'swftools_simpleviewer.admin.inc',
      'page argument' => 'swftools_simpleviewer_profile_form',
    ),
  );
  return array(
    'image_list' => array(
      'simpleviewer' => $simpleviewer,
    ),
  );
}

/**
 * Implements player pre-render function.
 */
function swftools_simpleviewer_simpleviewer_pre_render($element) {

  // Merge settings now if the playlist handler didn't to do this already
  if (!isset($element['#_settings'])) {
    $element = swftools_array_merge(swftools_simpleviewer_settings($element['#profile']), $element);
  }

  // Extract any relevant flickr settings from the $element['#othervars'] array
  // @todo This is to do with the input filter - come back and rewrite
  // $user_flickr = array_intersect_key($element['#othervars'], $settings['flickr']);

  // Merge user defined flickr parameters with default parameters
  //$flickr_vars = array_merge($settings['flickr'], $user_flickr);

  // If useFlickr is true then add the flickr settings to the array of xml parameters
  if ($element['#settings']['flickr']['useFlickr'] === 'true' || $element['#settings']['flickr']['useFlickr'] === 1) {

    // Extract any relevant xml settings from the $element['#othervars'] array
    // @todo This is to do with the input filter - come back and rewrite
    // $user_xml = array_intersect_key($element['#othervars'], $settings['xml']);
    // Merge user defined xml parameters with default parameters
    // $xml_vars = array_merge($settings['xml'], $user_xml);

    // Set useFlickr 'true'
    $xml_vars['useFlickr'] = 'true';

    // If a username was specified include this as flickrUserName
    if ($element['#settings']['flickr']['flickrUserName']) {
      $xml_vars['flickrUserName'] = $element['#settings']['flickr']['flickrUserName'];
    }

    // If flickrTags is not set, but tags is, then use tags
    // @todo This is to do with the input filter - come back and rewrite
    // if (!$element['#settings']['flickr']['flickrTags'] && isset($element['#othervars']['tags']) && $element['#othervars']['tags']) {
    //   $element['#settings']['flickr']['flickrTags'] = $element['#othervars']['tags'];
    // }

    // If tags were specified include them as flickrTags
    if ($element['#settings']['flickr']['flickrTags']) {
      $xml_vars['flickrTags'] = $element['#settings']['flickr']['flickrTags'];
    }

    // Create the xml definition
    $xml = theme('swftools_simpleviewer_playlist_wrapper', array('header' => $xml_vars, 'xml' => ''));

    // Attach the resulting xml to the element
    $element['#attached']['xml'] = array($xml);

    // Set the filename
    $element['#_file'] = 'swftools://playlist/' . $element['#cid'] . '.xml';

  }

  // Set the flashvar that will point to the xml playlist file built earlier
  $element['#flashvars']['galleryURL'] = file_create_url($element['#_file']);

  // Return the modified element
  return $element;

}

/**
 * Implements hook_swftools_playlist_[player]().
 */
function swftools_simpleviewer_swftools_playlist_simpleviewer(&$element, $playlist_children) {

  // Get the settings
  $settings = swftools_simpleviewer_settings($element['#profile']);

  // Merge the element and the settings together, letting the element values take precendent
  $element = swftools_array_merge($settings, $element);

  // Set a flag to show we already did this step so we don't repeat it in pre-render
  $element['#_settings'] = TRUE;

  // Extract any relevant xml settings from the $element['#othervars'] array
  // @todo This is to do with the input filter - come back and rewrite
  // $user_xml = array_intersect_key($element['#othervars'], $settings['xml']);

  // Merge user defined xml parameters with default parameters
  $xml_vars = $element['#settings']['xml'];

  // Initialise a string to contain the elements
  $xml = '';

  // Iterate over the playlist to build elements xml
  foreach ($playlist_children as $key) {
    $element['#playlist'][$key]['file'] = swftools_image_style_url($element['#settings']['image_style'], $element['#playlist'][$key]['file']);
    $xml .= theme('swftools_simpleviewer_playlist_element', $element['#playlist'][$key]);
  }

  // Add xml wrapper around the elements
  $xml = theme('swftools_simpleviewer_playlist_wrapper', array('header' => $xml_vars, 'xml' => $xml));

  // Attach the resulting xml
  $element['#attached']['xml'] = array($xml);

  // Set the filename
  $element['#file'] = 'swftools://playlist/' . $element['#cid'] . '.xml';

}

/**
 * Implements hook_theme().
 */
function swftools_simpleviewer_theme() {
  return array(
    'swftools_simpleviewer_playlist_element' => array(
      'template' => 'swftools-simpleviewer-playlist-element',
      'render element' => 'element',
    ),
    'swftools_simpleviewer_playlist_wrapper' => array(
      'template' => 'swftools-simpleviewer-playlist-wrapper',
      'render element' => 'element',
    ),
  );
}

/**
 * Returns the settings for SimpleViewer.
 *
 * Fetch the currently stored settings, or to return the default settings if
 * not yet customised.
 *
 * @param string $profile
 *    (optional) Name of profile to use.
 *
 * @return array
 *   An array of settings.
 */
function swftools_simpleviewer_settings($profile) {

  $defaults = array(
    '#height' => 600,
    '#width' => 700,
    '#settings' => array(
      'xml' => array(
        'textColor' => '#FFFFFF',
        'frameColor' => '#FFFFFF',
        'frameWidth' => 20,
        'galleryStyle' => 'MODERN',
        'thumbPosition' => 'LEFT',
        'thumbColumns' => 3,
        'thumbRows' => 4,
        'showOpenButton' => 'true',
        'showFullScreenButton' => 'true',
        'maxImageWidth' => 480,
        'maxImageHeight' => 480,
      ),
      'flickr' => array(
        'useFlickr' => 0,
        'flickrUserName' => '',
        'flickrTags' => '',
      ),
      'image_style' => SWFTOOLS_UNDEFINED,
    ),
  );
  $settings = swftools_variable_get('swftools_simpleviewer', $defaults, $profile);
  return $settings;
}

/**
 * Implements hook_swftools_actions().
 */
function swftools_simpleviewer_swftools_actions() {
  return array(
    'image_list' => array(
      '#description' => t('A series of images'),
      '#type' => 'image',
    ),
  );
}

/**
 * Process variables for swftools-simpleviewer-playlist-element.tpl.php.
 *
 * @see swftools-simpleviewer-playlist-element.tpl.php
 */
function template_preprocess_swftools_simpleviewer_playlist_element(&$variables) {
  $variables['title'] = check_plain($variables['title']);
}

/**
 * Process variables for swftools-simpleviewer-playlist-wrapper.tpl.php.
 *
 * @see swftools-simpleviewer-playlist-wrapper.tpl.php
 */
function template_preprocess_swftools_simpleviewer_playlist_wrapper(&$variables) {
  $variables['header'] = drupal_attributes($variables['header']);
  $variables['header'] = str_replace(array('#', '0x'), '', $variables['header']);
}
