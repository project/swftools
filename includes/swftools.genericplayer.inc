<?php

/**
 * @file
 * Enables SWF Tools support for a built-in generic mp3 player.
 */

/**
 * Although there are only actually two functions in this include file they are
 * not 'core' parts of SWF Tools. By placing them here they can easily be
 * stripped out in the future.
 */

/**
 * Implements hook_swftools_methods().
 */
function swftools_genericplayer_swftools_methods() {
  return array(
    'audio' => array(
      'generic_mp3' => array(
        'module' => 'swftools',
        'title' => t('Generic mp3 player'),
        'library' => drupal_get_path('module', 'swftools') . '/misc/generic_mp3.swf',
      ),
    ),
  );
}

/**
 * Implements hook_[player]_pre_render().
 */
function swftools_generic_mp3_pre_render($element) {
  $element['#flashvars']['file_url'] = file_create_url($element['#_file']);
  $element['#height'] = 31;
  $element['#width'] = 30;
  return $element;
}
