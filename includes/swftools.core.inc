<?php

/**
 * @file
 * Implements SWF Tools core functions that are common to the main module and the API module.
 *
 * Core functions in this file are needed by both the lightweight API, and the
 * full SWF Tools module.
 *
 * Core functions allow SWF Tools to:
 * - Retrieve a list of embedding methods from enabled modules
 * - Report the direct embedding method as available
 * - Theme content using the swftools_direct embedding method
 * - Theme elements of #type => 'swftools'
 * - Return constants defining the library, alternate content,
 *   and the default for adding JavaScript to all pages.
 */

/**
 * @addtogroup swftools
 * @{
 */

/**
 * JavaScript will be added to all pages when they are served, even if SWF Tools isn't called directly.
 */
define('SWFTOOLS_ALWAYS_ADD_JS', 0x0001);

/**
 * The default alternate markup string that is displayed if the embedding process fails.
 */
define('SWFTOOLS_DEFAULT_HTML_ALT', 'You are missing some Flash content that should appear here! Perhaps your browser cannot display it, or maybe it did not initialize correctly.');

/**
 * JavaScript will be added inline with the page markup.
 */
define('SWFTOOLS_JAVASCRIPT_INLINE', 0x0000);

/**
 * JavaScript will be added to the page header.
 */
define('SWFTOOLS_JAVASCRIPT_HEADER', 0x0001);

/**
 * JavaScript will be added to the page footer.
 */
define('SWFTOOLS_JAVASCRIPT_FOOTER', 0x0002);

/**
 * The default minimum Flash version
 */
define('SWFTOOLS_MINIMUM_FLASH_VERSION', 7);

/**
 * Collects information from all modules about the players and embedding methods available.
 *
 * @param string $specific_method
 *   (optional) Retrieve data only about a specific method.
 *
 * @return array
 *   Associative array of data describing the available methods.
 */
function swftools_get_methods($specific_method = NULL) {
  $methods = &drupal_static(__FUNCTION__);
  if (!isset($methods)) {
    if ($cache = cache_get('methods', 'cache_swftools')) {
      $methods = $cache->data;
    }
    else {
      $methods = array();
      foreach (module_implements('swftools_methods') as $module) {
        $implements = module_invoke($module, 'swftools_methods');
        if (isset($implements) && is_array($implements)) {
          foreach ($implements as $method_type => $method) {
            foreach ($method as $method_name => $method_details) {
              $method_details += array(
                'name'     => $method_name,
                'module'   => '',
                'title'    => '',
                'download' => '',
                'library'  => '',
                'profile'  => array(),
              );
              if ($method_type == 'embed') {
                $method_details += array(
                  'theme' => $method_name,
                );
              }
              else {
                $method_details += array(
                  'version'  => SWFTOOLS_MINIMUM_FLASH_VERSION,
                );
              }
              $methods[$method_type][$method_name] = $method_details;
            }
          }
        }
      }
      cache_set('methods', $methods, 'cache_swftools');
    }
  }

  /**
   * If a specific method was requested, but no module reported that method,
   * then the following line makes sure that NULL will be returned, avoiding
   * a PHP notice error.
   */
  if ($specific_method) {
    $methods += array(
      $specific_method => NULL,
    );
  }

  // Return results - either for the specific method, or the whole array
  return $specific_method ? $methods[$specific_method] : $methods;

}

/**
 * Returns a path to the requested library, using Libraries API when it is available.
 *
 * If the Libraries API is present then the library will be searched for under the
 * profiles/$profile/libraries, sites/all/libraries, and sites/sitename/libraries.
 *
 * sites/sitename/libraries has the highest priority and will "win" if it is present.
 *
 * If Libraries API is not present then the library should be present in
 * sites/all/libraries.
 *
 * @param string $library
 *   The name of the library to locate, e.g. jwplayer5.
 *
 * @return string
 *   The path to the requested library.
 *
 * @see http://drupal.org/project/libraries Libraries API
 */
function swftools_get_library_path($library) {

  // @todo Would there be any advantage to caching this result?
  return ($ret = module_invoke('libraries', 'get_path', $library)) ? $ret : 'sites/all/libraries/' . $library;

}

/**
 * Verifies that the supplied id is unique, and assigns an id if one was not
 * supplied.
 *
 * Typically this is called to get us a unique id, but the user can also
 * supply an id. In all cases we call drupal_html_id() to verify that the id is
 * unique in this page generation session. The entire $variables array is
 * passed by reference and modified directly.
 *
 * @param array $element
 *   The variables array contining the id to be checked.
 * @param bool $check_unique
 *   - TRUE: the id will be checked to make sure that it is unique.
 *   - FALSE: the id will not be checked for uniqueness so the user can be sure
 *     the id returned has not been modified unexpectedly. The caller is
 *     responsible for making sure ids are unique on the page.
 */
function swftools_set_id(&$element, $check_unique = TRUE) {
  $element['#id'] = $element['#id'] ? $element['#id'] : uniqid('swftools-');
  if ($check_unique) {
    $element['#id'] = drupal_html_id($element['#id']);
  }
}

/**
 * @} End of "addtogroup swftools".
 */

/**
 * Implements hook_swftools_methods().
 *
 * Note that we use a 'trick' here so that the direct embedding method is
 * always available whether the API or full SWF Tools modules are enabled.
 * We implement system_swftools_methods() as we know the system module
 * will always be present and therefore this hook will be called.
 */
function system_swftools_methods() {
  return array(
    'embed' => array(
      'swftools_direct' => array(
      'module' => 'swftools',
      'title' => t('Direct embedding'),
      ),
    ),
  );
}

/**
 * Returns the core element definition for an SWF Tools element.
 *
 * We declare it here since both the full SWF Tools modules and the API module
 * declare this element. The respective hooks can call for this array instead
 * of defining it in multiple locations.
 */
function _swftools_element_info() {
  return array(
      '#file' => NULL,
      '#id' => NULL,
      '#html_alt' => NULL,
      '#parameters' => array(),
      '#flashvars' => array(),
      '#embed' => NULL,
  );
}

/**
 * Customised version of drupal_http_build_query().
 *
 * drupal_http_build_query() implodes using &, which means a Flashvars string
 * appears to fail XHTML validation. This version implode using &amp; which will
 * pass XHMTL validation.
 *
 * http://jonahchanticleer.com/archives.cfm/category/from-red-to-green/page/1
 *
 * Issue originally raised in #1011402
 *
 * @param array $query
 *   The parameter array to be processed.
 * @param string $parent
 *   Internal use only. Used to build the $query array key for nested items.
 *
 * @return string
 */
function swftools_http_build_query(array $query, $parent = '') {
  $params = array();
  foreach ($query as $key => $value) {
    $key = ($parent ? $parent . '[' . rawurlencode($key) . ']' : rawurlencode($key));
    if (is_array($value)) {
      $params[] = swftools_http_build_query($value, $key);
    }
    elseif (!isset($value)) {
      $params[] = $key;
    }
    else {
      $params[] = $key . '=' . str_replace('%2F', '/', rawurlencode($value));
    }
  }
  return implode('&amp;', $params);
}
