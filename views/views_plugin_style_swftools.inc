<?php

/**
 * @file
 * Defines the SWF Tools Views plug-in.
 */

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */

class views_plugin_style_swftools extends views_plugin_style {

  /**
   * Forces the file and image fields to be formatted as plain text using
   * the swftools_plain formatter.
   */
  function init(&$view, &$display, $options = NULL) {
    parent::init(&$view, &$display, $options = NULL);
    if ($this->options['file'] != SWFTOOLS_UNDEFINED) {
      $this->view->field[$this->options['file']]->options['type'] = 'swftools_plain';
    }
    if ($this->options['image'] != SWFTOOLS_UNDEFINED) {
      $this->view->field[$this->options['image']]->options['type'] = 'swftools_plain';
    }
  }

  /**
   * Modifies a basic query by adding a WHERE clause to match thumbnails with
   * their associated media file, and to accommodate the case where there is
   * not a thumbnail for every item of content.
   */
  function query() {

    // We only need this modification if there are both files and images
    if ($this->options['file'] != SWFTOOLS_UNDEFINED && $this->options['image'] != SWFTOOLS_UNDEFINED) {

      $file = $this->view->field[$this->options['file']]->table;
      $this->view->query->add_field($file, 'delta');

      $image = $this->view->field[$this->options['image']]->table;
      $thumbnail = $image . '.' . $this->view->field[$this->options['image']]->real_field;
      $this->view->query->add_field($image, 'delta');

      $where = "($file.delta = $image.delta) OR $thumbnail IS NULL OR $thumbnail = '' OR $thumbnail = 0";

      $this->view->query->add_where_expression(0, $where);

    }

  }

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['profile'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['file'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['image'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['title'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['description'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['author'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['date'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['link'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['duration'] = array('default' => SWFTOOLS_UNDEFINED);
    $options['height'] = array('default' => '');
    $options['width'] = array('default' => '');
    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Get handlers
    $handlers = $this->display->handler->get_handlers('field');

    // If there are no handlers then show a message
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#prefix' => '<div class="error messages">',
        '#markup' => t('You need at least one field before you can configure your SWF Tools settings'),
        '#suffix' => '</div>',
      );
    }
    else {
      $form['intro_markup'] = array(
        '#prefix' => '<div class="form-item description">',
        '#markup' => t('Configure the playback of this content by mapping fields and specifying player settings below. As a minimum a field must be mapped to <em>File</em>. The field that is mapped to <em>File</em> must provide a valid path to the content, with no HTML markup. Note that not all players support all playlist elements and options.'),
        '#suffix' => '</div>',
      );

      // Initialise profile options with SWF Tools default
      $options = array(
        SWFTOOLS_UNDEFINED => t('SWF Tools default'),
      );

      // Populate with profiles if swftools_profiles is available
      if (function_exists('swftools_profiles_get_profiles')) {
        $profiles = swftools_profiles_get_profiles();
        foreach ($profiles as $key => $info) {
          $options[$key] = $info['name'];
        }
      }

      // Initialise an array of fields for the select lists
      $fields = array(
        SWFTOOLS_UNDEFINED => t('None'),
      );

      // Add the available fields to the select list, formatted with their short name
      foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
        $fields[$field] = $handler->ui_name(TRUE);
      }

      // Files
      $form['file'] = array(
        '#type' => 'select',
        '#title' => t('File'),
        '#description' => t('Select the field that will be used to construct the playlist - these are the files that will actually play.'),
        '#options' => $fields,
        '#default_value' => $this->options['file'],
      );

      // This pre-render function is needed to enable fieldsets
      $form['#pre_render'][] = 'views_ui_pre_render_add_fieldset_markup';

      $form['more_fields'] = array(
        '#type' => 'fieldset',
        '#title' => t('More field mappings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 99,
      );

      // Thumbnails
      $form['image'] = array(
        '#type' => 'select',
        '#title' => t('Thumbnail'),
        '#description' => t('Select the field that will be used to supply thumbnails for each file in the playlist.'),
        '#options' => $fields,
        '#default_value' => $this->options['image'],
        '#fieldset' => 'more_fields',
      );

      // Titles
      $form['title'] = array(
        '#type' => 'select',
        '#title' => t('Title'),
        '#description' => t('Select the field that should be rendered as the title for each file.'),
        '#options' => $fields,
        '#default_value' => $this->options['title'],
        '#fieldset' => 'more_fields',
      );

      // Descriptions
      $form['description'] = array(
        '#type' => 'select',
        '#title' => t('Description'),
        '#description' => t('Select the field that should be rendered as the description for each file.'),
        '#options' => $fields,
        '#default_value' => $this->options['description'],
        '#fieldset' => 'more_fields',
      );

      // Authors
      $form['author'] = array(
        '#type' => 'select',
        '#title' => t('Author'),
        '#description' => t('Select the field that should be rendered as the author for each file.'),
        '#options' => $fields,
        '#default_value' => $this->options['author'],
        '#fieldset' => 'more_fields',
      );

      // Dates
      $form['date'] = array(
        '#type' => 'select',
        '#title' => t('Date'),
        '#description' => t('Select the field that should be rendered as the date for each file.'),
        '#options' => $fields,
        '#default_value' => $this->options['date'],
        '#fieldset' => 'more_fields',
      );

      // Link
      $form['link'] = array(
        '#type' => 'select',
        '#title' => t('Link'),
        '#description' => t('Select the field that should be rendered as the link for each file.'),
        '#options' => $fields,
        '#default_value' => $this->options['link'],
        '#fieldset' => 'more_fields',
      );

      // Duration
      $form['duration'] = array(
        '#type' => 'select',
        '#title' => t('Duration'),
        '#description' => t('Select the field that should be rendered as the duration for each file.'),
        '#options' => $fields,
        '#default_value' => $this->options['duration'],
        '#fieldset' => 'more_fields',
      );

      $form['more_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('More settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 100,
      );

      // Add a select list to choose the profile
      $form['profile'] = array(
        '#type' => 'select',
        '#title' => t('Playlist handler'),
        '#description' => t('Select the profile that you want to use to display this playlist. If no profiles have been created then the default SWF Tools file handling and player settings will be used.'),
        '#options' => $options,
        '#default_value' => $this->options['profile'],
        '#fieldset' => 'more_settings',
      );

      // Appearance - height
      $form['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => $this->options['height'],
        '#description' => t('The height of the player - leave blank to use the player or profile defaults.'),
        '#fieldset' => 'more_settings',
      );

      // Appearance - width
      $form['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => $this->options['width'],
        '#description' => t('The width of the player - leave blank to use the player or profile defaults.'),
        '#fieldset' => 'more_settings',
      );

    }
  }

  /**
   * Render the display in SWF Tools style.
   *
   * Over-ride default render as we output directly from drupal_render().
   */
  function render($vars) {

    // If the file mapping hasn't been then set an error
    if ($this->options['file'] == SWFTOOLS_UNDEFINED) {
      $element = array(
        '#prefix' => '<div class="error messages">',
        '#markup' => t('You must map a field to the file path before the playlist can be created.'),
        '#suffix' => '</div>',
      );
      return drupal_render($element);
    }
    else {

      // Get the fields we are using
      $keys = array_keys($this->view->field);

      // Initialise a playlist ready for SWF Tools
      $playlist = array();

      // Initialise array of elements for the playlist
      $elements = array(
        'file',
        'title',
        'image',
        'description',
        'author',
        'date',
        'link',
        'duration',
      );

      // Render the fields, results go in to rendered_fields
      $this->render_grouping($this->view->result, $this->options['grouping']);

      // Iterate over the results
      // @todo We want the raw data - can we get to that without a theme?
      foreach ($this->view->style_plugin->rendered_fields as $key => $row) {

        // Reset the work array on each pass
        $work = array();

        // Attach fields that have been defined
        foreach ($elements as $element) {
          $playlist[$key][$element] = ($this->options[$element] != SWFTOOLS_UNDEFINED) && $row[$this->options[$element]] ? $row[$this->options[$element]] : '';
        }

      }

      // Make an SWF Tools element
      $element = array(
        '#type' => 'swftools',
        '#playlist' => $playlist,
      );

      // Assign the relevant profile
      if ($this->options['profile'] != SWFTOOLS_UNDEFINED) {
        $element['#profile'] = $this->options['profile'];
      }

      // Override height and width if they were set
      if ($this->options['height'] !== '') {
        $element['#height'] = $this->options['height'];
      }
      if ($this->options['width'] !== '') {
        $element['#width'] = $this->options['width'];
      }

      /**
       * If the view isn't working as expected then it can be very informative to see
       * what is coming through to the playlist for rendering. Uncomment the line
       * below to dump the element definition prior to rendering.
       */
//       $element = array('#markup' => print_r($element, true));

    }

    // Return the rendered element
    return drupal_render($element);

  }

}
