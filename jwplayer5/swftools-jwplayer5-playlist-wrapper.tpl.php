<?php

/**
 * @file
 * Template for the xml wrapper around a LongTail JW Player 5 playlist.
 */
?>
<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/" xmlns:jwplayer="http://developer.longtailvideo.com/">
  <channel>
    <title>SWF Tools Playlist</title>
    <?php print $xml; ?>
  </channel>
</rss>
