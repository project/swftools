<?php

/**
 * @file
 * Configuration settings for LongTail JW Player 5.
 */

module_load_include('inc', 'swftools', 'includes/swftools.admin');

/**
 * Form definition: Standard (non-profile) settings form
 */
function swftools_jwplayer5_admin_form() {
  $form = swftools_jwplayer5_profile_form();
  // @todo Complaints that this slows down sites - don't do it automatically?
//  $form['#submit'][] = 'swftools_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Submit handler to prepend # on to the top level keys and merge behavior
 * related settings in to a single bit mask.
 */
function swftools_jwplayer5_admin_form_submit($form, &$form_state) {

  // Combine behavior related settings in to single bit mask
  $form_state['values']['swftools_jwplayer5']['behavior'] =
    (SWFTOOLS_BEHAVIOR_AUTOSTART & $form_state['values']['swftools_jwplayer5']['flashvars']['behavior_autostart']) |
    (SWFTOOLS_BEHAVIOR_REPEAT & $form_state['values']['swftools_jwplayer5']['flashvars']['behavior_repeat']) |
    (SWFTOOLS_BEHAVIOR_HIDE_CONTROLS & $form_state['values']['swftools_jwplayer5']['flashvars']['behavior_hide_controls']);

  // Unset the variables we used to get behaviors
  unset($form_state['values']['swftools_jwplayer5']['flashvars']['behavior_autostart']);
  unset($form_state['values']['swftools_jwplayer5']['flashvars']['behavior_repeat']);
  unset($form_state['values']['swftools_jwplayer5']['flashvars']['behavior_hide_controls']);

  // Prefix each primary key with a #
  foreach ($form_state['values']['swftools_jwplayer5'] as $key => $value) {
    $form_state['values']['swftools_jwplayer5']['#' . $key] = $value;
    unset($form_state['values']['swftools_jwplayer5'][$key]);
  }

}

/**
 * Autocompleter to suggest possible skin names.
 *
 * @param string $string
 *   The partial name of the skin.
 *
 * @return array
 *   An array of possible matches.
 */
function swftools_jwplayer5_autocomplete_skin($string = '') {

  // Only do something if there is a string to be matched
  if ($string) {

    // Scan the mediaplayer5/skins directory for swf files
    $skins = file_scan_directory(swftools_get_library_path('mediaplayer5') . '/skins', '\.swf$');

    // Build an array skin names
    $possible_values = array();
    foreach ($skins as $skin) {
      $possible_values[] = $skin->basename;
    }

    // Find matches
    $matches = array();
    foreach ($possible_values as $value) {
      if (preg_match("/$string/i", $value)) {
        $matches[$value] = $value;
      }
    }

    print drupal_json_encode($matches);
  }

}

/**
 * Form definition: JW Player 5 settings.
 *
 * @return array
 *   A form definition array
 */
function swftools_jwplayer5_profile_form($profile = '') {

  /**
   * Retrieve current settings
   */
  $settings = swftools_jwplayer5_settings($profile);

  // See if colorpicker 2 is loaded
  // @todo - reinstate color picker?
//  $colorfield = function_exists('colorpicker_2_or_later') ? 'colorpicker_textfield' : 'textfield';

  // Initialise tree from this point forward as want to store arrays
  $form['swftools_jwplayer5']['#tree'] = TRUE;

  // This pre-render function is needed to enable fieldsets
  $form['#pre_render'][] = 'swftools_pre_render_fieldsets';

  // Define fieldsets used for presentation purposes
  $form['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 50,
  );

  $form['behavior'] = array(
    '#type' => 'fieldset',
    '#title' => t('Behavior'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 51,
  );

  $form['logo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 52,
  );

  $form['colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Colors'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 53,
  );

  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 54,
  );

  $form['swftools_jwplayer5']['flashvars']['controlbar.position'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['controlbar.position'],
    '#title' => t('Control bar position'),
    '#options' => array(
      'default' => 'default',
      'bottom' => 'bottom',
      'over' => 'over',
      'none' => 'none',
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['behavior_hide_controls'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#behavior'] & SWFTOOLS_BEHAVIOR_HIDE_CONTROLS,
    '#title' => t('Player controls'),
    '#options' => array(
      0 => t('Visible'),
      SWFTOOLS_BEHAVIOR_HIDE_CONTROLS => t('Hidden'),
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['controlbar.idlehide'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['controlbar.idlehide'],
    '#title' => t('Controls when player is idle'),
    '#options' => array(
      'false' => t('Visible'),
      'true' => t('Hidden'),
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['display.showmute'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['display.showmute'],
    '#title' => t('Mute button'),
    '#options' => array(
      'true' => t('Show'),
      'false' => t('Hide'),
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['dock'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['dock'],
    '#title' => t('Plugin buttons'),
    '#options' => array(
      'true' => t('Show'),
      'false' => t('Hide'),
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['icons'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['icons'],
    '#title' => t('Play button and buffer icons'),
    '#options' => array(
      'true' => t('Show'),
      'false' => t('Hide'),
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['playlist.position'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['playlist.position'],
    '#title' => t('Playlist position'),
    '#options' => array(
      'bottom' => t('Bottom'),
      'top' => t('Top'),
      'right' => t('Right'),
      'left' => t('Left'),
      'over' => t('Over'),
      'none' => t('None'),
    ),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['playlist.size'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['playlist.size'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Playlist size'),
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['skin'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['skin'],
    '#title' => t('Skin'),
    '#autocomplete_path' => 'admin/config/media/swftools/jwplayer5/autocomplete',
    '#fieldset' => 'layout',
  );

  $form['swftools_jwplayer5']['flashvars']['behavior_autostart'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#behavior'] & SWFTOOLS_BEHAVIOR_AUTOSTART,
    '#title' => t('Autostart'),
    '#options' => array(
      SWFTOOLS_BEHAVIOR_AUTOSTART => t('Enabled'),
      0 => t('Disabled'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['bufferlength'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['bufferlength'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Buffer length'),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['mute'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['mute'],
    '#title' => t('Mute on startup'),
    '#options' => array(
      'true' => t('Enabled'),
      'false' => t('Disabled'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['plugins'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['plugins'],
    '#title' => t('Plugins'),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['behavior_repeat'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#behavior'] & SWFTOOLS_BEHAVIOR_REPEAT,
    '#title' => t('Repeat'),
    '#options' => array(
      SWFTOOLS_BEHAVIOR_REPEAT => t('Enabled'),
      0 => t('Disabled'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['repeat'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['repeat'],
    '#title' => t('Repeat type'),
    '#options' => array(
      'none' => t('None'),
      'list' => t('List'),
      'always' => t('Always'),
      'single' => t('Single'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['shuffle'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['shuffle'],
    '#title' => t('Shuffle'),
    '#options' => array(
      'true' => t('Enabled'),
      'false' => t('Disabled'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['smoothing'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['smoothing'],
    '#title' => t('Smoothing'),
    '#options' => array(
      'true' => t('Enabled'),
      'false' => t('Disabled'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['stretching'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['stretching'],
    '#title' => t('Stretching'),
    '#options' => array(
      'none' => t('None'),
      'exactfit' => t('Exact fit'),
      'uniform' => t('Uniform'),
      'fill' => t('Fill'),
    ),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['volume'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['volume'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Volume'),
    '#fieldset' => 'behavior',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.file'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['logo.file'],
    '#title' => t('Logo file'),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.link'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['logo.link'],
    '#title' => t('Logo link'),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.linktarget'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['logo.linktarget'],
    '#title' => t('Logo link target'),
    '#options' => array(
      '_self' => t('Self'),
      '_blank' => t('Blank'),
      '_parent' => t('Parent'),
      '_top' => t('Top'),
    ),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.hide'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['logo.hide'],
    '#title' => t('Hide logo after playback starts'),
    '#options' => array(
      'true' => t('Enabled'),
      'flase' => t('Disabled'),
    ),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.margin'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['logo.margin'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Logo margin'),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.position'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#flashvars']['logo.position'],
    '#title' => t('Logo position'),
    '#options' => array(
      'bottom-left' => t('Bottom left'),
      'bottom-right' => t('Bottom right'),
      'top-left' => t('Top left'),
      'top-right' => t('Top right'),
    ),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.timeout'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['logo.timeout'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Logo timeout'),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.over'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['logo.over'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Logo alpha on mouseover'),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['logo.out'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['logo.out'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Logo alpha on no mouseover'),
    '#fieldset' => 'logo',
  );

  $form['swftools_jwplayer5']['flashvars']['backcolor'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['backcolor'],
    '#size' => 8,
    '#maxlength' => 6,
    '#title' => t('Background color (control bar and playlist)'),
    '#fieldset' => 'colors',
  );

  $form['swftools_jwplayer5']['flashvars']['frontcolor'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['frontcolor'],
    '#size' => 8,
    '#maxlength' => 6,
    '#title' => t('Front color (icons and text)'),
    '#fieldset' => 'colors',
  );

  $form['swftools_jwplayer5']['flashvars']['lightcolor'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['lightcolor'],
    '#size' => 8,
    '#maxlength' => 6,
    '#title' => t('Light color (mouseover controls)'),
    '#fieldset' => 'colors',
  );

  $form['swftools_jwplayer5']['flashvars']['screencolor'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#flashvars']['screencolor'],
    '#size' => 8,
    '#maxlength' => 6,
    '#title' => t('Screen color (background color of display)'),
    '#fieldset' => 'colors',
  );

  $form['swftools_jwplayer5']['height'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#height'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Height'),
    '#fieldset' => 'other',
  );

  $form['swftools_jwplayer5']['width'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['#width'],
    '#size' => 8,
    '#maxlength' => 5,
    '#title' => t('Width'),
    '#fieldset' => 'other',
  );

  $form['swftools_jwplayer5']['accessible'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#accessible'],
    '#title' => t('Accessibility controls'),
    '#options' => array(
      SWFTOOLS_ACCESSIBLE_DISABLED => t('Disabled'),
      SWFTOOLS_ACCESSIBLE_VISIBLE => t('Enabled and visible'),
      SWFTOOLS_ACCESSIBLE_HIDDEN => t('Enabled and hidden'),
    ),
    '#fieldset' => 'other',
  );

  $form['swftools_jwplayer5']['settings']['fillemptyimages'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#settings']['fillemptyimages'],
    '#title' => t('Fill empty images'),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
    '#fieldset' => 'other',
  );

  $image_styles = array(SWFTOOLS_UNDEFINED => t('None'));
  foreach(image_styles() as $style => $details) {
    $image_styles[$style] = $details['name'];
  }

  $form['swftools_jwplayer5']['settings']['image_style_player'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#settings']['image_style_player'],
    '#title' => t('Image style for player thumbnails'),
    '#options' => $image_styles,
    '#fieldset' => 'other',
  );

  $form['swftools_jwplayer5']['settings']['image_style_playlist'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#settings']['image_style_playlist'],
    '#title' => t('Image style for playlist thumbnails'),
    '#options' => $image_styles,
    '#fieldset' => 'other',
  );

  $form['swftools_jwplayer5']['settings']['use_jwplayer_embedding'] = array(
    '#type' => 'select',
    '#default_value' => $settings['#settings']['use_jwplayer_embedding'],
    '#title' => t('JW embedding'),
    '#options' => array(
      'flash' => t('Enabled - Flash preferred'),
      'html5' => t('Enabled - HTML5 preferred'),
      0 => t('Disabled'),
    ),
    '#fieldset' => 'other',
  );

  //  // Add custom form handler to prepend # on to top level keys
  $form['#submit'][] = 'swftools_jwplayer5_admin_form_submit';

  // Return the form
  return $form;

}
