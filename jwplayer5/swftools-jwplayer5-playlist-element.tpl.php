<?php

/**
 * @file
 * Template for a single entry in a LongTail JW Player 5 playlist.
 */
?>
  <item>
    <title><?php print $title; ?></title>
    <description><?php print $description; ?></description>
    <media:content url="<?php print $file; ?>" />
    <?php if ($image) : ?>
      <media:thumbnail url="<?php print $image; ?>" />
    <?php endif; ?>
    <media:credit><?php print $author; ?></media:credit>
    <?php if ($duration) : ?>
      <jwplayer:duration><?php print $duration; ?></jwplayer:duration>
    <?php endif; ?>
    <?php if ($stream) : ?>
      <jwplayer:provider><?php print $provider; ?></jwplayer:provider>
      <?php if ($streamer) : ?>
        <jwplayer:streamer><?php print $streamer; ?></jwplayer:streamer>
      <?php endif; ?>
    <?php endif; ?>
  </item>
