Key changes in SWF Tools 7:

- Rather than calling a theme SWF Tools now requires that an element is used.
  The element type is 'swftools', and it takes a series of parameters that
  define the content to be rendered. As a minimum the element definition
  needs to include:
  - #type: swftools
  - #file: the url to the file - PHP streams are supported, so public://xxx
    or private://xxx are valid, as are http://xxx and so on; or
  - #playlist: an array of items to assemble in to a playlist

- Other parameters for the element mostly use the equivalent names from the
  original SWF Tools 'options' array. All these parameters are optional and if
  they are not specified SWF Tools will assign appropriate values.
  The available parameters are as follows:
  - #id: element id (if the caller wants to force a specific id)
  - #html_alt: HTML alternative content
  - #parameters: associative array of Flash parameters
  - #flashvars: associative array of flashvars as key/value pairs
  - #action: action to take with the specified file(s)
  - #player: player to use to play the specified file(s)
  - #embed: embed method to use (will over-ride the default method)
  - #height: Flash movie height
  - #width: Flash movie width
  - #attributes: associative array of attributes to attach to the element
  - #return: when specified, and TRUE, return the cache id of the SWF Tools
    content, rather than returning the content markup.

- Because the content is now treated as an element it can be included in a form
  definition, and it can include parameters like #prefix and #suffix to easily
  wrap other markup around the content.

- To render the content via PHP rather than calling print swf(file, options)
  you define the element, then call print drupal_render(element).

- Calls to the swf() function are still supported. All swf() does is turn the
  incoming string, or array, in to an element. At present it will not pass
  any associated data from the 'options' array.

- The render flow of an SWF Tools element is as follows and commences when
  drupal_render() is called for an 'swftools' element:
  - Call swftools_pre_render(). This is the entry point to SWF Tools and does
    the bulk of the work. It determines the player, action, embed method, cid,
    height and width as necessary.
  - Because we do not know in advance what player might be used the function
    will try to call the associated hook_[player]_pre_render() function to allow
    any manipulation of the element and its data (e.g. preparing flashvars).
  - Classes needed by SWF Tools to enabled JavaScript interaction are added,
    and the id is added.
  - A standard SWF Tools wrapper is added by using #theme_wrappers.
  - The element property #theme is set to the appropriate embedding theme for
    actual output of the element. The function will call any associated
    hook_[embed]_pre_render() function to allow manipulation as needed to suit
    the final theme requirements.
  - If the user requested just the CID of the content then the entire element
    is replaced by a simple markup element with that value.
  - Otherwise the element is returned and it is now 'fully formed' and ready to
    be themed.
  - The actual conversion of an element to markup is done within standard theme
    functions. These now do very little processing as they assume that all the
    pre-processing has been done elsewhere (via the pre-render) functions.

- It is possible to call the theme functions directly, e.g.
  theme_swftools_direct(), but the final output functions assume that the
  element they are receiving is 'complete'.

- The swftools_api module is a stripped down version of SWF Tools. It provides
  access to the embedding methods via an element of type 'swftools_api'. These
  elements receive minimal pre-processing and rely on the caller to properly
  describe the content they want to have rendered. Therefore they must provide
  a proper URL on the #file key, and a sufficient set of other parameters. All
  the pre-render will do for this element is set a height and width of 100% if
  the caller didn't specify anything, set the embedding theme, and call the
  embedding pre-render function, if it exists.

- SWF Tools exposes formatters to the Field API. The default formatters are
  'SWF Tools - separate files' and 'SWF Tools - playlist'.

The following aspects of SWF Tools 6 are deprecated:

- Legacy action names are no longer supported. Only the new names should be
  used when specifying actions, players or embedding methods.

- Height and width can no longer being passed as a string on the 'params'
  key as WIDTHxHEIGHT. They must only be passed on 'othervars'['height'] and
  'othervars'['width'].

- Height and width can no longer be passed on 'flashvars'. They must only be
  passed on 'othervars'['height'] and 'othervars'['width'].

- Height and width can no longer be passed on 'params'. They must only be
  passed on 'othervars'['height'] and 'othervars'['width'].

- Flashvars can no longer be passed to theme('swftools', $variables) as a
  string. An array of key/value pairs must be used.

- Support for remote media path via swftools_media_url removed. This is no
  longer needed with the advent of stream wrappers. If the file is specified
  with a wrapper then Drupal will automatically expand it to the proper
  location.
